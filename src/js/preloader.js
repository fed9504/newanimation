let preloader = document.getElementById("preloader");
let vid = document.getElementById("bgvideo");

let urlsVideo = [
  {
    url: "./img/background.mp4",
    id: "bgvideo",
  },
  {
    url: "./img/Ice_cream.mp4",
    id: "bokeh3",
  },
  {
    url: "./img/Italy.mp4",
    id: "bokeh4",
  },
];


let progress = 0;
function loadVideo(item) {
  return new Promise(function (resolve, reject) {
    let xhrReq = new XMLHttpRequest();
    xhrReq.open("GET", item.url, true);
    xhrReq.responseType = "blob";
    xhrReq.onload = function () {
      if (this.status === 200) {
        // let video = document.getElementById(item.id);
        let vid = URL.createObjectURL(this.response);
        // video.src = vid;
        resolve(vid);
      }
    };
    xhrReq.onerror = function () {
      console.log("err", arguments);
      reject(err);
    };

    xhrReq.onprogress = function (e) {
      if (e.lengthComputable) {
        let persent = (((e.loaded / e.total) * 100) | 0);
        let percentComplete = persent + "%";
        // let complete = persent;
        // persentInt = Math.floor(20/100 * persent);


        // console.log("progress: ", percentComplete);
        if (percentComplete === "100%") {
          progress += 33;
          preloader.innerHTML = progress +"%";
        }
        if (progress == "99") {
          $("#preloader_overlay").fadeOut(1500);
        }
      }
    };
    xhrReq.send();
  });
}

urlsVideo.map((el) => {
  loadVideo(el).then((res) => {
    console.log("res", res);
    let video = document.getElementById(el.id);
    video.src = res;
  });
});
