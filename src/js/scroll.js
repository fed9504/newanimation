import SmoothScroll from './smoot'


SmoothScroll({
    frameRate: 70,
    animationTime: 900,
    stepSize: 100,
    pulseAlgorithm: 1,
    pulseScale: 4,
    pulseNormalize: 1,
    accelerationDelta: 40,
    accelerationMax: 1,
    keyboardSupport: 1,
    arrowScroll: 20,
    fixedBackground: 0
});